void main() {
  Student sd = new Student();

  sd.name = "Weeraphat";
  sd.surname = "Songsrijan";
  sd.gpa = 3.99;

  sd.display();
}

class Student {
  late String _name, _surname;
  late double _gpa;

  Student(){}

  set name(String n) => _name = n;
  set surname(String s) => _surname = s;
  set gpa(double g) => _gpa = g;

  String get name => _name;
  String get surname => _surname;
  double get gpa => _gpa;

  void display() {
    print("Name: $name $surname\n GPA: $gpa");
  }
}
